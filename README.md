# README #

This is a collection of Physica scripts, used for loading and fitting beta-detected NMR (beta-NMR) data files from TRIUMF. To run the scripts simple run physica (http://computing.triumf.ca/legacy/physica/), and then start the main script @bnmrfit

Version 1.3.6

For questions please contact Zaher Salman <zaher.salman@psi.ch>