!
!
!  *** BNMRFIT.PCM ***
!
!
clear
clear\alphanumeric
autoscale on
window 12 0 0 100 80

! Fit Parameters
resout=`results'                ! The name of fit results file 
binfac=10

xlbl=`Temperature [K]'
ylbl=`Freq [KHz]'
xvar=2
yvar=4
yerrvar=5

! Colors
cdate=`<C10>'                   ! green, color for date in title
ctitle=`<C6>'                   ! blue, color for title
cfit=`<C7>'                     ! purple nurple, color for fit results 

display `===================================================================='
display `=                 Welcome to Physica -- BNMRFIT                    ='
display `=                          Version 1.3.2                           ='
display `===================================================================='


dir_2=`./'
txt=` '
isdaqstruct=0                   ! Test runs are available 1 or not 0 
died=0
fnorm=0                         ! Normalization by neutral beam flag (0 off, 1 on)
m2e=0                           ! No mode selected for 2e
                                ! or Normalization of Forward counter by neutral beam (low field BNMR)

! Dead time correction flag initially turned off
dtflag=0
deadtime=.0000000450020547

! Number of binning ranges
nrange=1

!-----------------------------------------------------------------
inq1:
skewtoggle = 0
txt=`r'
inquire `>>' txt
! Accept both real and test (on isdaq01 only) run numbers
if (eqs(txt[1],`4')|(eqs(txt[1],`3')&(isdaqstruct=1))) then
! Check if txt is a number of 5 digits
  txtchar=ichar(txt)
  if (clen(txt)=5) then
    if (min((txtchar>=48)&(txtchar<=57))) then
      RUN=eval(txt)
      txt=`r'
      goto run
    endif
  endif
endif
txt=txt//` '
if (eqs(lcase(txt[1]),`h')|eqs(txt[1],`?')) then goto help
if (eqs(lcase(txt[1]),`f')&eqs(txt[2],`d')) then goto correction
if (eqs(lcase(txt[1]),`p')) then goto prn
if (eqs(lcase(txt[1]),`k')) then goto kil
if (eqs(lcase(txt[1]),`f')) then goto fit
if (eqs(lcase(txt[1]),`w')) then goto skewness
if (eqs(lcase(txt[1]),`r')) then goto run
if (eqs(lcase(txt[1]),`m')) then goto mode
if (eqs(lcase(txt[1]),`s')) then goto sel
if (eqs(lcase(txt[1]),`x')) then goto axis
if (eqs(lcase(txt[1]),`c')) then goto col
if (eqs(lcase(txt[1]),`v')) then goto avg
if (eqs(lcase(txt[1]),`b')) then goto bin
if (eqs(lcase(txt[1]),`i')) then goto info
if (eqs(lcase(txt[1]),`a')) then goto ascii
if (eqs(lcase(txt[1]),`y')) then goto chyear
if (eqs(lcase(txt[1]),`z')) then goto wsize
if (eqs(lcase(txt[1]),`d')) then goto deadtime
if (eqs(lcase(txt[1]),`l')) then goto plot
if (eqs(lcase(txt[1]),`q')) then goto quit

goto inq1  

!-----------------------------------------------------------------
run:                            ! Read run number to analyse
@dir_2//`bnmr_init'
@dir_2//`bnmr_getrun'
display `This run took '//rchar(elapsedsec/60)//` minutes to acquire'
! Calculates the asymmetry
@dir_2//`bnmr_asy'
!-----------------------------------------------------------------
if (died = 1) then
  died=0                        ! Reset value 
  goto inq1
endif
draw:
clear
@dir_2//`bnmr_graph' 
goto inq1
!-----------------------------------------------------------------
help:
display `Summary of commands:'
display `h/?   - show this Help'
display `4xxxx - Load run number 4xxxx'
display `a     - save data to Ascii file'
display `b     - Bin data'
display `c     - Colors redifinition' 
display `d     - turn on/off Deadtime correction' 
display `fd    - Find Deadtime correction' 
display `f     - Fit data to function'
display `i     - show Info about this run (e.g. T)'
display `k     - Kill or remove a bad scan'
display `p     - Print graphics window'
display `r     - Reload current run or load a new run'
display `s     - Select combination of scans'
display `v     - aVerage and RMS of selected region'
display `w     - calculate skeWness and related normalized statistics'
display `x     - aXis rescale (0 0 0 0 for autoscale)'
display `y     - change the Year of data source'
display `z     - resiZe physica window'
display `l     - pLot fit results from file'
display `q     - Quit BNMRFIT'
display ` '
display ` '
goto inq1
!-----------------------------------------------------------------
prn:                            ! Print the figure
hard
goto inq1
!-----------------------------------------------------------------
kil:                            ! Remove bad scans
remtxt=` '
remscan=0
inquire `Remove scan number (Separated by ";" for multiple scans) >>' remtxt
if (nes(remtxt,` ')|nes(remtxt[1],`0')) then
  remtxt=`[0;'//remtxt//`]'
  tmpremscans=eval(remtxt)
  if (tmpremscans[2] ~= 0) then
    do i=[2:len(tmpremscans)]
      nremscan=nremscan+1
      remscans[nremscan]=tmpremscans[i]
    enddo
  endif
endif
goto draw
!-----------------------------------------------------------------
mode:
txt=`1'
display `Choose mode for asymmetry calculation'
display `1- (F-B)/(F+B)'
display `2- F/(NBF+NBB)'
display `3- B/(NBF+NBB)'
display `4- Single Counter'
display `5- (NBF-NBB)/(NBF+NBB)'
display `6- General Asymmetry from 4 counters'
display `7- 2a mode Differences'
display `8- 2e mode Differences'
display `9- 2e mode Slopes'
display `10- 2e mode (A-C)/(A+C)'
display `11- 2e mode A-C/(2B)'
display `12- 2e mode (A-B)/(A+B)+(B-C)/(B+C)'
inquire `Select mode ['//rchar(fnorm+1)//`] >>',txt
if (txt>=1&txt<=7) then 
  fnorm=txt-1
  m2e=0
  txt=`r'
  goto run
endif
if (txt>=8&txt<=12) then 
  m2e=txt-1
  txt=`r'
  goto run
endif
goto inq1
!-----------------------------------------------------------------
correction:                     ! Find best deadtime correction
@dir_2//`bnmr_fdt'         
goto inq1
!-----------------------------------------------------------------
deadtime:                       ! Turn on/off deadtime correction
if (dtflag=0) then
  display `Dead time correction turned ON'
  inquire `Dead time is ['//rchar(deadtime)//`] >>',deadtime
endif
if (dtflag=1) then
  display `Dead time correction turned OFF'
endif
dtflag=abs(dtflag-1)
txt=`r'
goto run
goto inq1
!-----------------------------------------------------------------
sel:                            ! Select combination of helicities
@dir_2//`bnmr_selector'         
goto draw
!-----------------------------------------------------------------
wsize:                           ! Resize physica window
replot
resize
goto inq1
!-----------------------------------------------------------------
axis:                           ! Rescale axis
inquire `Enter minx maxx miny maxy >>' minx maxx miny maxy
scale minx maxx miny maxy
if ((minx=0)&(maxx=0)&(miny=0)&(maxy=0)) then 
  autoscale on
endif
replot
goto inq1
!-----------------------------------------------------------------
col:                            ! Select a region and average on it
inquire `Current date color ['//cdate//`] >>' cdate
inquire `Current title color ['//ctitle//`] >>' ctitle
inquire `Current fit resulats color ['//cfit//`] >>' cfit
goto inq1
!-----------------------------------------------------------------
avg:                            ! Select a region and average on it
@dir_2//`bnmr_selector'         
@dir_2//`bnmr_average'
goto inq1
!-----------------------------------------------------------------
fit:                            ! Fit one, two or three peaks
getsig:
inquire `Number of peaks/components (1-8) ['//rchar(sig)//`]? >>' sig
if (sig<1 | sig>8) then 
  display `Choose 1-8 peaks/components! Sorry, limited by PHYSICA'
  goto getsig
endif

if (eqs(method,methoda)|eqs(method,methodd)) then
!   Lorentzian or Gaussian
  ftype:
  display `1- Lorentzian'
  display `2- Gaussian'
  display `3- Erf'
  display `4- Lorentzian/Gaussian Combo'
  inquire `Fit to ['//ftn//`] >>' ftn
! this is for lines with different types
  ftypetxt=`['//ftn//`;0]'
  ftype=eval(ftypetxt)
  if (len(ftype)=2 & sig>1) then ftype[1:sig]=ftype[1]
  if (sig>len(ftype)) then
    display `Bad input! try again'
    goto ftype
  endif
  fix:
!   Initialize, no fixed paremeters
!  if (exist(`fixp')) then destroy fixp
!  if (exist(`fixt')) then destroy fixt
!  if (exist(`fixv')) then destroy fixv
  if (~exist(`fixpar')) then fixpar = 0
  inquire `Number of parameters to fix (['//rchar(fixpar)//`])?' fixpar
  if (fixpar > 24 ) then  
    display `That cannot be right, try again'
    goto fix
  endif
  if (fixpar > 0) then
!     Get the fixed parameters input
    display `The structure of input to fix parameters is the following:'
    display `Peak# Type# Value'
    display ` Peak# - is the number of the peak'
    display ` Type# - is 1 for Amp, 2 for Res, 3 for Wid or Bi, and 4 for BCK or Alpha'
    display ` Value - is the value of the parameter to be fixed'
    do nfixpar = [1:fixpar]
!       Create three arrays which contain peak number, parameter type, and 
!       fixed value of each fixed parameter
      oldvalue = ` '
      if (exist(`fixp')) then
        if (len(fixp)>=nfixpar) then 
          oldvalue = rchar(fixp[nfixpar])//` '//rchar(fixt[nfixpar])//` '//rchar(fixv[nfixpar])
          npeak = fixp[nfixpar]
          ntype = fixt[nfixpar]
          pvalue = fixv[nfixpar]
        endif
      endif
      inquire `Parameter # '//rchar(nfixpar)//` ['//oldvalue//`]>> ' npeak ntype pvalue
! Array for peak number
      fixp[nfixpar]=npeak
! Array for parameter type
      fixt[nfixpar]=ntype
! Array for parameter value
      fixv[nfixpar]=pvalue
    enddo
  endif
endif

if (eqs(method,methodb)|eqs(method,methodc)) then
!   Exponential od Stretch
  ftypees:
  display `1- Exponential'
  display `2- Stretch Exponential'
  display `3- Exponential with Beam On/Off'
  inquire `Fit to ['//ftn//`] >>' ftn
! this is for relaxation with different types
  ftypetxt=`['//ftn//`;0]'
  ftype=eval(ftypetxt)
  if (len(ftype)=2 & sig>1) then ftype[1:sig]=ftype[1]
  if (sig>len(ftype)) then
    display `Bad input! try again'
    goto ftypees
  endif
  fixes:
!   Initialize, no fixed paremeters
!  if (exist(`fixp')) then destroy fixp
!  if (exist(`fixt')) then destroy fixt
!  if (exist(`fixv')) then destroy fixv
  if (~exist(`fixpar')) then fixpar = 0
  inquire `Number of parameters to fix (['//rchar(fixpar)//`])?' fixpar
  if (fixpar > 24 ) then  
    display `That cannot be right, try again'
    goto fixes
  endif
  if (fixpar > 0) then
!     Get the fixed parameters input
    display `The structure of input to fix parameters is the following:'
    display `Component# Type# Value'
    display ` Component# - is the number of the component'
    display ` Type#      - is 1 for A, 2 for Lam, and 3 for B'
    display ` Value      - is the value of the parameter to be fixed'
    do nfixpar = [1:fixpar]
!       Create three arrays which contain peak number, parameter type, and 
!       fixed value of each fixed parameter
      oldvalue = ` '
      if (exist(`fixp')) then
        if (len(fixp)>=nfixpar) then 
          oldvalue = rchar(fixp[nfixpar])//` '//rchar(fixt[nfixpar])//` '//rchar(fixv[nfixpar])
          npeak = fixp[nfixpar]
          ntype = fixt[nfixpar]
          pvalue = fixv[nfixpar]
        endif
      endif
      inquire `Parameter # '//rchar(nfixpar)//` ['//oldvalue//`] >> ' npeak ntype pvalue
! Array for peak number
      fixp[nfixpar]=npeak
! Array for parameter type
      fixt[nfixpar]=ntype
! Array for parameter value
      fixv[nfixpar]=pvalue
    enddo
  endif
endif



!   Get initial guess    
@dir_2//`bnmr_inputpar'

do scan = [1:compscans]
!   Skip removed scans
  if (max(scan=remscans)=1&cselec=0) then goto skiprem
!   set up arrays for fitting
  ix = bfrq
  iyasy = basy[scan,*]
  iyerr = baerr[scan,*]
  if (dselec=1) then            ! select the requested subset of the scan
    ix=ix[selrange]
    iyasy=iyasy[selrange]
    iyerr=iyerr[selrange]
    scanlen=len(ix)
  endif

!   Plot guess
  guess:
  @dir_2//`bnmr_guess'
  txt=`yes'
  inquire `Is it a good guess [(yes)/no]? >>' txt
  colour 0
  set pchar 0
  graph\noaxes `Guess' ix guessy
  
  if (eqs(txt[1],`n')) then 
    @dir_2//`bnmr_inputpar'
    goto guess
  endif
    
!   fit
  @dir_2//`bnmr_fit'

!   Calculate and display statistics of normalized function
  if (skewtoggle = 1) then
    @dir_2//`bnmr_fitstat'
    goto inq1
  endif

!   Export params to log files
  if (eqs(method,methoda)|eqs(method,methodd)) then
    if (nes(rchar(amp1),rchar(0/0))) then
      txt=`no'
      inquire `Write fit parameters to file [y/(n)]? >>' txt
      if (eqs(txt[1],`y')) then
        inquire `What is the value and error in the independent variable? ['//rchar(indp)//` '//rchar(indperr)//`]' indp indperr
        inquire `What file name should I use? ['//resout//`]' resout
!         Write first part of values as initial values for next time
        write\text dir_2//`bnmr_fitpar.pcm' `SIG='//rchar(sig)
!        write\text\append dir_2//`bnmr_fitpar.pcm' `FTN=`'//ftn

        npar=2
        do i = [1:sig]
          line=rchar(runnumber)//` '//rchar(indp)//` '//rchar(indperr)//` '
          do j= [1:3]
            skipp=0
            if (fixpar>0) then
              do k=[1:fixpar]
                if ((fixp[k]=i)&(fixt[k]=j)) then
                  skipp=1
                  if (j = 1) then
                    line=line//rchar(eval(`AMP'//rchar(i)))//` '//rchar(0,frmt[2])//` '
                    write\text\append dir_2//`bnmr_fitpar.pcm' `AMP'//rchar(i)//`='//rchar(eval(`AMP'//rchar(i)))
                  endif
                  if (j = 2) then
                    line=line//rchar(eval(`RES'//rchar(i)))//` '//rchar(0,frmt[2])//` '
                    write\text\append dir_2//`bnmr_fitpar.pcm' `RES'//rchar(i)//`='//rchar(eval(`RES'//rchar(i)))
                  endif
                  if (j = 3) then
                    line=line//rchar(eval(`WID'//rchar(i)))//` '//rchar(0,frmt[2])//` '
                    write\text\append dir_2//`bnmr_fitpar.pcm' `WID'//rchar(i)//`='//rchar(eval(`WID'//rchar(i)))
                  endif
                endif
              enddo
            endif
            if (fixpar=0|skipp=0) then
              line=line//rchar(fit$var[npar])//` '//rchar(fit$e2(npar),frmt[1])//` '
              write\text\append dir_2//`bnmr_fitpar.pcm' fit$var[npar]//`='//rchar(fit$var[npar])
              npar=npar+1
            endif
          enddo
          line=line//rchar(bck)//` '//rchar(fit$e2(1),frmt[1])
          write\text\append dir_2//`outdata/'//resout//`.out'//rchar(i) line
!           Write second part of values as initial values for next time
        enddo


      endif
    endif
    if (eqs(rchar(amp1),rchar(0/0))) then 
      display `Fit diverging, not saving parameters'
      @dir_2//`bnmr_fitpar'
    endif

    pros:
    txt=`yes'
    inquire `proceed [print/(yes)/no] >>' txt
    if (eqs(txt[1],`n')) then goto inq1
    if (eqs(txt[1],`p')) then
      hard
      goto pros
    endif
    
    skiprem:
!     amplitude sign alternates between scans
    do i = [1:sig]
      fit$var[3*i-1]=(-1)*fit$var[3*i-1]
    enddo
!     Shift the baseline to a reasonable value
    if (scan<compscans) then bck=basy[scan+1,startpoint]
  enddo
endif

if (eqs(method,methodb)|eqs(method,methodc)) then
  if (nes(rchar(A0),rchar(0/0))) then
    txt=`n'
    inquire `Write fit parameters to file [y/(n)]? >>' txt
    if (eqs(txt[1],`y')) then
      inquire `What is the value and error in the independent variable? ['//rchar(indp)//` '//rchar(indperr)//`]>>' indp indperr

!       Write first part of values as initial values for next time
      write\text dir_2//`bnmr_fitpar.pcm' `SIG='//rchar(sig)

      npar=1
      do i = [0:sig-1]
        line=rchar(runnumber)//` '//rchar(indp)//` '//rchar(indperr)//` '
!	Number of fit parameters
	if (ftype[i]=1) then nparfit=2
	if (ftype[i]=2) then nparfit=3
	if (ftype[i]=3) then nparfit=2
        do j= [1:nparfit]
          skipp=0
          if (fixpar>0) then
            do k=[1:fixpar]
              if ((fixp[k]=i)&(fixt[k]=j)) then
                skipp=1
                if (j = 1) then
                  line=line//rchar(eval(`A'//rchar(i-1)))//` '//rchar(0,frmt[2])//` '
                  write\text\append dir_2//`bnmr_fitpar.pcm' `A'//rchar(i-1)//`='//rchar(eval(`A'//rchar(i-1)))
                endif
                if (j = 2) then
                  line=line//rchar(eval(`lam'//rchar(i-1)))//` '//rchar(0,frmt[2])//` '
                  write\text\append dir_2//`bnmr_fitpar.pcm' `Lam'//rchar(i-1)//`='//rchar(eval(`lam'//rchar(i-1)))
                endif
                if (j = 3) then
                  line=line//rchar(eval(`B'//rchar(i-1)))//` '//rchar(0,frmt[2])//` '
                  write\text\append dir_2//`bnmr_fitpar.pcm' `B'//rchar(i-1)//`='//rchar(eval(`B'//rchar(i-1)))
                endif
              endif
            enddo
          endif
          if (fixpar=0|skipp=0) then
            line=line//rchar(fit$var[npar])//` '//rchar(fit$e2(npar))//` '
            write\text\append dir_2//`bnmr_fitpar.pcm' fit$var[npar]//`='//rchar(fit$var[npar])
            npar=npar+1
          endif
        enddo
      enddo
      write\text\append dir_2//`outdata/'//resout//`_td.out'//rchar(i) line
    endif
  endif
  if (eqs(rchar(A0),rchar(0/0))) then 
    display `Fit diverging, not saving parameters'
  endif
endif
    
goto inq1

!-----------------------------------------------------------------
bin:
dselec=0
@dir_2//`bnmr_binnew'        ! Also adapted from Jess
goto draw
!-----------------------------------------------------------------
info:
if (len(indvarmean)=0) then 
  display `OLD RUN.. No info was stored'
  goto inq1
endif

do ivar=[1:len(indvarmean)]
  todisp = ` '
  if (nes(indvardescription[ivar],` ')) then 
    todisp=todisp//indvardescription[ivar]
    if (abs(indvarmean[ivar]) > 1e-15) then todisp=todisp//`    '//rchar(indvarmean[ivar])
    if (indvarstddev[ivar] > 1e-19) then todisp=todisp//`+-'//rchar(indvarstddev[ivar])
    if (nes(indvarunits[ivar],` ')) then todisp=todisp//`['//indvarunits[ivar]//`]'
    display todisp
  endif
enddo
goto inq1

!-----------------------------------------------------------------
ascii:                          ! Exporting data to ascci file
display `Write Data to ASCII file for external processing:'
fname=dir_2//`outdata/'//rchar(runnumber)//`.dat'
display `file = '//fname
if (eqs(method,methodb)|eqs(method,methodc)) then
  display `Note: Time t=0 coincides with the beginning of the pulse'
endif

! make a big matrix
data[1,1:scanlen]=bfrq[1:scanlen]
do scan=[1:compscans]
  data[2*scan,1:scanlen]=BASY[scan,1:scanlen]
  data[2*scan+1,1:scanlen]=BAERR[scan,1:scanlen]
enddo 
write\matrix fname <-data
if (exist(`data')) then destroy data
goto inq1
!-----------------------------------------------------------------
skewness:
skewtoggle = 1
display `Skewness calculation turned ON'
goto fit
!-----------------------------------------------------------------
chyear:
if (~exist(`year')) then year=`2004'
inquire `Take data from the year ['//year//`]>>' year

goto inq1
!-----------------------------------------------------------------
plot:
@dir_2//`bnmr_plres'
goto inq1
!--------------------------------------------------------------------
quit:
clear\alphanumeric
disable border 
!============================================================================
